---
title: Privacy policy
summary: How we handle your privacy in this website.
date: "2022-08-14T00:00:00Z"

reading_time: false  # Show estimated reading time?
share: false  # Show social sharing links?
profile: false  # Show author profile?
comments: false  # Show comments?

# Optional header image (relative to `assets/media/` folder).
#header:
#  caption: ""
#  image: ""
---

## Introduction

This website is operated by Gil Oliveira from the Portuguese Republic. 

This Privacy Policy explains the personal data we collect from you when you use this website. By using this website, you are subject to the terms of this privacy policy.

## Applicable law

[Regulation (EU) 2016/679 of the European Parliament and of the Council](https://eur-lex.europa.eu/eli/reg/2016/679/oj) (General Data Protection Regulation - GDPR) and [Law 58/2018, of the 8 of August](https://dre.pt/dre/detalhe/lei/58-2019-123815982) regulate the handling of personal data of this websites' users.

## What data do we collect?

We do not collect personal data from the users of this website.

## How do we collect your data?

This website does not utilise any tracking technology, such as cookies, to track its users.

## How do we store your data?

No storage of personal data from the users of this website is performed.

## What are your data protection rights?

EU and Portuguese law give all users of this website a series of rights concerning their personal data, regardless of their jurisdiction. We have a legal obligation to inform you of these rights.

Every user is entitled to the following:

- **The right to access** – You have the right to request copies of your personal data. We may charge you a small fee for this service.

- **The right to rectification** – You have the right to request that we correct any information you believe is inaccurate. You also have the right to request us to complete the information you believe is incomplete.

- **The right to erasure** – You have the right to request the erasure of your personal data, under certain conditions.

- **The right to restrict processing** – You have the right to request that we restrict the processing of your personal data, under certain conditions.

- **The right to object to processing** – You have the right to object to the processing of your personal data, under certain conditions.

- **The right to data portability** – You have the right to request the transfer the data that we have collected to another organization, or directly to you, under certain conditions.

If you make a request, we have one month to respond. If you wish to exercise any of these rights, please get in contact.

## Cookies

Cookies are text files placed on your computer to collect standard Internet log information and visitor behaviour information.

We do not employ cookies in this website.

## Hosting and data location

This website is hosted in Germany, and hosting is provided by a third party - [Contabo GmbH](https://contabo.com/), all services are provided within the European Union.

## Privacy policies of other websites

The website contains links to other websites. Our privacy policy applies only to our website, so if you click on a link to another website, you should read their privacy policy.

## Chnages to our privacy policy

We keep our privacy policy under regular review and place any updates on this web page. This privacy policy was last updated on September 10th, 2022, and is effective immediately. This policy can change at any moment.

## How to contact us

If you have any questions about our privacy policy, the data we hold on you, or you would like to exercise one of your data protection rights, please do not hesitate to contact us.

Email us at: [gil@giloliveira.net](mailto:gil@giloliveira.net) ([PGP key](/asc/keys.asc)).

## How to contact the appropriate authority

Should you wish to report a complaint or if you feel that we have not addressed your concern in a satisfactory manner, you may contact the appropriate authority.

**Name:** CNPD - Comissão Nacional de Proteção de Dados

**Address:** Av. D. Carlos I, 134, 1º, 1200-651 Lisbon, Portugal

**Phone:** (+351) 213 928 400

**Fax:** (+351) 213 976 832

**Email:** [geral@cnpd.pt](mailto:geral@cnpd.pt)

**Website:** [www.cnpd.pt](https://www.cnpd.pt)
